const getSum = (str1, str2) => {
  if (typeof str1 === 'object' || typeof str2 === 'object' || typeof str1 === 'array' || typeof str2 === 'array') {
    return false;
  }

  if (str1 === '') {
    str1 = 0;
  }
  if (str2 === '') {
    str2 = 0;
  }
  if (!/^\d+$/.test(str1) || !/^\d+$/.test(str2)) {
    return false;
  }
  let sum = +str1 + +str2;
  return sum.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let authors = listOfPosts.filter(post => post['author'] === authorName);
  let comments = [];
  listOfPosts.forEach(post => {
    if (post['comments']) {
      post['comments'].forEach(comment => {
        if (comment['author'] === authorName) {
          comments.push(comment);
        }
      })
    }
  })
  return `Post:${authors.length},comments:${comments.length}`;
};

const tickets = (people) => {
  people = people.map(el => +el);

  let [n25, n50, n100] = [0, 0, 0];
  for (let el of people) {
    switch (el) {
      case 25:
        n25++;
        break;
      case 50:
        n50++;
        n25--;
        break;
      case 100:
        n100++;
        n25--;
        if (n50) {
          n50--;
        } else {
          n25 -= 2;
        }
        break;
    }
    if ([n25, n50, n100].some(v => v < 0)) {
      return 'NO';
    }
  }
  return 'YES';
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
